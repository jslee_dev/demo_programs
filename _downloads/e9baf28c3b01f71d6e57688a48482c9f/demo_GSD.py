"""
--------------------------------------------------------------------------------
Evolution of Grain Size Distribution (GSD),
Numerical solution of continuity equation for size distribution function

                                                                    Jae Sang Lee
                                                                     2023.08.08b
--------------------------------------------------------------------------------
"""

import numpy as np
import matplotlib.pyplot as plt

R_max = 500e-6
iM = 300
ntime_max = 100000; ntime_mon = 10000

# ==============================================================================
def main_GSD():
    dR = R_max/iM
    R = np.zeros(iM+2)
    f = np.zeros(iM+2)
    f0 = np.zeros(iM+2)

    # IC, a bell-shape profile
    for i in range(1,iM+1):
        R[i] = (i-0.5)*dR
        f0[i] = 1e20*(((R[i]-0.2*R_max)**2)*((R[i]-0.5*R_max)**2)) if (R[i]>0.2*R_max and R[i]<0.5*R_max) else 0
    #

    # plot initial phi-profile
    plt.plot(R[1:iM],f0[1:iM], label="initial")

    dt = 1e-6 # arbitrary value, dt need careful setting!
    # time-loop <<------------------------------------------------------------
    for ntime in range(1,ntime_max+1): # 1 ~ ntime_max
        # BC
        f0[0] = 0.0
        f0[iM+1] = 0.0
        # calc. f(t+dt) from f(t)
        R_cr = np.sum(R*f0*dR)/np.sum(f0*dR) # R_cr = R_mean (number based) in 2-D 
        for i in range(1,iM+1): # 1 ~ iM
            f_dR_dt_w = 0.5*(f0[i-1]+f0[i]) * dR_dt(0.5*(R[i-1]+R[i]), R_cr)
            f_dR_dt_e = 0.5*(f0[i]+f0[i+1]) * dR_dt(0.5*(R[i]+R[i+1]), R_cr)
            df_dt = (f_dR_dt_w - f_dR_dt_e)/dR
            f[i] = f0[i] + df_dt * dt
        #
        f0 = np.copy(f)
        #
        if(ntime%ntime_mon==0):
            print("ntime = {}, R_cr = {:13.5e}".format(ntime, R_cr))
            #xxx print("{:13.5e}".format(np.sum((R[1:iM+1]**2)*f[1:iM+1])))
            plt.plot(R[1:iM],f[1:iM], label="ntime = {}".format(ntime))
            plt.legend(bbox_to_anchor=(0.6, 1.0), loc='upper left', borderaxespad=0.)
            plt.show(block = False); plt.pause(1.e-2)
        #
    #
    # time-loop >>------------------------------------------------------------

    print("End of calculation...")
    plt.show()
#



# ==============================================================================
def dR_dt(R, R_cr):
    k = 1e-6 # arbitrary value, k need careful setting!
    v = k*(1./R_cr - 1./R)
    return v
#



# ==============================================================================
if __name__ == "__main__":
    main_GSD()
