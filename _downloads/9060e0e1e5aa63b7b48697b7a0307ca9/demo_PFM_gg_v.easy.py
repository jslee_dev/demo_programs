"""
--------------------------------------------------------------------------------
A Minimal Implementation of Multi-Phase-Field Model for Grain Growth

                                                                    Jae Sang Lee
                                                                      2023.05.29
Note!
- This Python code is for demonstration purposes and is provided as-is 
  without warranty of any kind. 
- Equations and procedures are implemented in a straightforward manner 
  for readability. 
- For fast calculations, please optimize the procedures and 
  make a C/C++ or Fortran code.
- For larger systems, a large number of phases is necessary for the phase-field,
  and sparse arrays (selective allocation) should be used as in ref[2].
- For more information, refer to the following articles.
  [1] I.Steinbach and F.Pezzollar, Physica D 134 (1999), 385.
  [2] S.G.Kim et.al., Phy.Rev.E 74 (2006), 061605.
  [3] J.S.Lee et.al. Scrpita Mater. 110 (2016), 113.
--------------------------------------------------------------------------------
"""

import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

# input & setting data
# iM = 128; jM = 128; phM = 3     # for a circular grain (ph = 2) in matrix (ph = 3)
# iM = 128; jM = 128; phM = 4     # for a circular grain (ph = 2) at GB
iM = 128; jM = 128; phM = 2     # for anti-phase boundary
# iM = 128; jM = 128; phM = 32    # for polycrystalline grain growth
dx = 0.2e-6; dy = dx            # [m]
Lambda = 2.5*dx                 # [m], half thickness of interface
sigma_GB = 0.5                  # [J/m^2], GB energy
m_GB = 1.e-10                   # [(m/s)/(J/m^3)], GB mobility
ntime_max = 1000; ntime_mon = 50

# ==============================================================================
def main_pfm_gg():
    # parameters in calculation
    eps = 4/np.pi * np.sqrt(Lambda*sigma_GB)
    w = 2.*sigma_GB/Lambda
    M = np.pi**2/16 * m_GB/Lambda
    dt = 0.7* 1./(2*(eps*eps*M)*(1./(dx*dx)+1./(dy*dy)))
    TOL_phi = 1.e-3
    np.random.seed(0) # for reproducible random numbers

    # variables
    phi0 = np.zeros((iM+2,jM+2,phM+1), dtype=np.float64)    # phase-field at t
    phi  = np.zeros((iM+2,jM+2,phM+1), dtype=np.float64)    # phase-field at t+dt
    # aux. variables
    laplacian = np.zeros(phM+1, dtype=np.float64)           # laplacian
    is_exist = np.empty(phM+1)                              # for checking existing phases

    # for plot
    val2D = np.zeros((iM+2,jM+2), dtype=np.float64)
    plt.style.use("dark_background")                        
    fig_map, ax_map = plt.subplots(1, 1); 


    # IC <<---------------------------------------------------------------
    # IC, random phases of ph = 1 ~ phM in cubic array
    seed_size = 4
    for i in range(1,iM+1,seed_size):
        for j in range(1,jM+1,seed_size):
            ph = np.random.randint(1,phM+1)
            for ii in range(seed_size):
                for jj in range(seed_size):
                    phi0[i+ii,j+jj,ph] = 1.0 
                #
            #
        #
    #

    t = 0 #var init
    # IC >>---------------------------------------------------------------

    # time-loop <<--------------------------------------------------------
    for ntime in tqdm(range(1,ntime_max+1)): # ntime = 1 ~ ntime_max
        t = t + dt

        # periodic BC along i = 0 and iM+1
        for j in range(1,jM+1): # 1 ~ jM
            for ph in range(1,phM+1): # 1 ~ phM
                phi0[0,j,ph] = phi0[iM,j,ph]
                phi0[iM+1,j,ph] = phi0[1,j,ph]
            #
        #
        # periodic BC along j = 0 and jM+1
        for i in range(1,iM+1): # 1 ~ iM
            for ph in range(1,phM+1): # 1 ~ phM
                phi0[i,0,ph] = phi0[i,jM,ph]
                phi0[i,jM+1,ph] = phi0[i,1,ph]
            #
        #

        # internal points of (1~iM, 1~jM) <<------------------------------
        for i in range(1,iM+1): # 1 ~ iM
            for j in range(1,jM+1): # 1 ~ jM

                # check co-existing phases and prepare laplacian
                n_coexist = 0 #var init
                for ph in range(1,phM+1): # 1 ~ phM
                    is_exist[ph] = False #var init
                    laplacian[ph] = 0. #var init
                    tmp = phi0[i,j,ph] + (phi0[i-1,j,ph] + phi0[i+1,j,ph] + phi0[i,j-1,ph] + phi0[i,j+1,ph])
                    if(tmp > TOL_phi):
                        n_coexist = n_coexist + 1 # maximum of n_coexist <= ~6 in relaxed systems
                        is_exist[ph] = True
                        laplacian[ph] = (phi0[i+1,j,ph] - 2.*phi0[i,j,ph] + phi0[i-1,j,ph])/(dx*dx) + \
                                        (phi0[i,j+1,ph] - 2.*phi0[i,j,ph] + phi0[i,j-1,ph])/(dy*dy)
                    #
                #

                # calc. phi at t+dt
                for ph in range(1,phM+1): # 1 ~ phM
                    if(is_exist[ph] == True and n_coexist>=2):
                        sum = 0. #var init
                        for qh in range(1,phM+1): # 1 ~ phM
                            if(is_exist[qh] == True and qh != ph):
                                term = -(0.5*eps*eps*(laplacian[ph]- laplacian[qh]) + w*(phi0[i,j,ph]-phi0[i,j,qh]))
                                sum = sum + M*term
                            #
                        #
                        dphi_over_dt = -(2./n_coexist)*sum
                        phi[i,j,ph] = phi0[i,j,ph] + dphi_over_dt*dt
                        # limit phi (remove overflow due to double-obstacle potential)
                        if  (phi[i,j,ph]>(1.0-TOL_phi)): phi[i,j,ph] = 1.0
                        elif(phi[i,j,ph]<TOL_phi): phi[i,j,ph] = 0.0
                    #
                    else:
                        phi[i,j,ph] = phi0[i,j,ph]
                    #
                #

                # normalize phi
                sum_tmp = 0 #var init
                for ph in range(1,phM+1): # 1 ~ phM
                    sum_tmp = sum_tmp + phi[i,j,ph]
                #
                for ph in range(1,phM+1): # 1 ~ phM
                    phi[i,j,ph] = phi[i,j,ph]/sum_tmp
                #

            #
        #
        # internal points of (1~iM, 1~jM) <<------------------------------

        # phase-field for the next time step
        for i in range(1,iM+1): # 1 ~ iM
            for j in range(1,jM+1): # 1 ~ jM
                for ph in range(1,phM+1): # 1 ~ phM
                    phi0[i,j,ph] = phi[i,j,ph] 
                #
            #
        #

        # monitoring <<---------------------------------------------------
        if(ntime==5 or ntime%ntime_mon == 0):
            tqdm.write("ntime = {:d}, t = {:13.5e}".format(ntime, t)) # tqdm() 사용시 tqdm.write() 로 출력
            # decorate GB regions and plot
            for i in range(1,iM+1): # 1 ~ iM
                for j in range(1,jM+1): # 1 ~ jM
                    val_tmp = 0 #var init
                    for ph in range(1,phM+1):
                        val_tmp = val_tmp + phi[i,j,ph]**4
                    #
                    val2D[i,j] = val_tmp
                #
            #
            colormesh_val(fig_map, ax_map, val2D, 1,iM, 1,jM, 0.,1., "ntime = {:d}".format(ntime)) # plot (1~iM, 1~jM) 2D matrix data
        #
        # monitoring >>---------------------------------------------------
    #
    # time-loop >>--------------------------------------------------------

    print("OK! End of calculation...")
    plt.show() # for persisting plot
#



# ==============================================================================
def colormesh_val(fig, ax, val2D, il, ih, jl, jh, val_min, val_max, suptitle): 
    """
    plot (il~ih, jl~jh) 2D matrix data
    """
    fig.suptitle("{}".format(suptitle))
    x, y = np.meshgrid(np.arange(il,ih+1), np.arange(jl,jh+1))
    # plot transposed val2D to make i-axis rightward and j-axis upward
    ax.pcolormesh(x, y, np.transpose(val2D[il:ih+1,jl:jh+1]), vmin = val_min, vmax = val_max, shading='auto', cmap='Blues_r') # cmap='RdBu_r', 'Blues_r', 'Greys_r'
    ax.set_aspect('equal', 'box')
    plt.savefig("{}.png".format(suptitle))
    plt.show(block = False); plt.pause(1.e-2)
#



# ==============================================================================
if __name__ == "__main__":
    main_pfm_gg()



# ==============================================================================
"""

iM = 128; jM = 128; phM = 3     # a flat and a sinusoidal boundaries
    # IC, a flat and a sinusoidal boundary
    for i in range(1,iM+1): # 1 ~ iM
        for j in range(1,jM+1): # 1 ~ jM
            i_flat = 0.25*iM
            i_wave = 0.65*iM + 10*np.cos(2*np.pi*(j-1)/(jM-1))
            if (i>=i_flat and i<=i_wave):
                phi0[i,j,2] = 1.0
            else:
                phi0[i,j,3] = 1.0
            #
        #
    #


iM = 128; jM = 128; phM = 3     # for a circular grain (ph = 2) in matrix (ph = 3)
    # IC, a circular grain (ph = 2) in matrix (ph = 3)
    i_center = int(0.4*iM); j_center = int(0.6*jM); nradius = int(0.2*iM)
    for i in range(1,iM+1): # 1 ~ iM
        for j in range(1,jM+1): # 1 ~ jM
            if ((i-i_center)**2+(j-j_center)**2 <= nradius**2):
                phi0[i,j,2] = 1.0
            else:
                phi0[i,j,3] = 1.0
            #
        #
    #


iM = 128; jM = 128; phM = 4 # for a circular grain (ph = 2) at GB
    # IC, a circular grain (ph = 2) at GB 
    i_center = int(0.5*iM); j_center = int(0.5*jM); nradius = int(0.2*iM)
    for i in range(1,iM+1): # 1 ~ iM
        for j in range(1,jM+1): # 1 ~ jM
            if ((i-i_center)**2+(j-j_center)**2 <= nradius**2):
                phi0[i,j,2] = 1.0
            else:
                if(j<=j_center):
                    phi0[i,j,3] = 1.0
                else:
                    phi0[i,j,4] = 1.0
                #
            #
        #
    #


iM = 128; jM = 128; phM = 2     # for anti-phase boundary
iM = 128; jM = 128; phM = 32    # for polycrystalline grain growth
    # IC, random phases of ph = 1 ~ phM in cubic array
    seed_size = 4
    for i in range(1,iM+1,seed_size):
        for j in range(1,jM+1,seed_size):
            ph = np.random.randint(1,phM+1)
            for ii in range(seed_size):
                for jj in range(seed_size):
                    phi0[i+ii,j+jj,ph] = 1.0 
                #
            #
        #
    #

"""