"""
--------------------------------------------------------------------------------
xi-vector plot and Wulff construction (on x-y plane)

                                                                    Jae Sang Lee
                                                                      2023.06.12
- For more information on xi-vector, refer to the following articles.
  [1] D.W.Hoffman and J.W.Cahn, Surface Science 31 (1972), 368.
  [2] J.W.Cahn and D.W.Hoffman, Acta Metall.Mater. 22 (1974), 1205.
--------------------------------------------------------------------------------
"""

import numpy as np
import matplotlib.pyplot as plt

def g():
    # dummy function for global var.
    g.alpha = 0.05 # 0.2 #1./15
    g.phi = 0.0 #var init
    g.is_cubic = True; g.is_hexagonal = (not g.is_cubic)
#
g()
sigma0 = 1.0
fz = 1.0



def main_xi_plot():
    theta = np.pi/2; phi_min = 0.0; phi_max = 2*np.pi
    n_phi = 900 + 1
    phi_list = np.linspace(phi_min, phi_max, n_phi)

    # sigma, xi, sigma_inv arrays by array operations
    rx = np.sin(theta)*np.cos(phi_list)
    ry = np.sin(theta)*np.sin(phi_list)
    rz = np.cos(theta)*np.ones_like(phi_list) # need for array operations
    (sigma_x_list, sigma_y_list, sigma_z_list) = get_vec_sigma(rx, ry, rz)
    (xi_x_list, xi_y_list, xi_z_list) = get_vec_xi(rx, ry, rz)
    #
    fig, ax = plt.subplots(1,1, figsize=(7,7))
    fig.subplots_adjust(bottom=0.2)
    ax.set_aspect('equal', 'box')
    plt.xlabel("x"); plt.ylabel("y")
    plt.xlim(-1.5, 1.5); plt.ylim(-1.5, 1.5)
    plt.plot([-1.5, +1.5, np.nan, 0.0, 0.0, ],[0.0, 0.0, np.nan, -1.5, +1.5], c="#999999", lw=0.5) # x, y 축

    line_sigma_, = ax.plot(sigma_x_list, sigma_y_list, c="#000000", lw=2.0, label=r"$\sigma/\sigma_0$") # sigma plot
    line_xi_,  = ax.plot(xi_x_list, xi_y_list, c="#FF0000", lw=1.0, label=r"$\xi/\sigma_0$") # xi plot
    text_ = ax.text(-1.35, 1.3, r"$\alpha = {:g}$".format(g.alpha), fontsize=15)

    phi_deg = 0.
    (sigma_x, sigma_y, sigma_z), (xi_x, xi_y, xi_z) = get_sigma_and_xi(phi_deg)
    segment_sigma, = plt.plot([0.0, sigma_x], [0.0, sigma_y], "-o", c="#000000", lw=1.0, markersize=3)
    segment_xi, = plt.plot([0.0, xi_x], [0.0, xi_y], "-o", c="#FF0000", lw=1.0, markersize=3)


    # widgets for GUI <<--------------------------------------------------
    from matplotlib.widgets import Slider, RadioButtons
    slider_alpha_axis = fig.add_axes([0.3, 0.07, 0.5, 0.03])
    slider_phi_axis = fig.add_axes([0.3, 0.01, 0.5, 0.03])
    slider_alpha = Slider(slider_alpha_axis, label=r"$\alpha$   ",
                    valmin=0.0, valmax=0.3, valinit=g.alpha, valstep=0.01)
    slider_phi = Slider(slider_phi_axis, label=r"$\phi [^o]$   ",
                    valmin=0.0, valmax=90.0, valinit=phi_min, valstep=1)

    def update_line(alpha):
        g.alpha = alpha
        (sigma_x_list, sigma_y_list, sigma_z_list) = get_vec_sigma(rx, ry, rz)
        (xi_x_list, xi_y_list, xi_z_list) = get_vec_xi(rx, ry, rz)
        #
        line_sigma_.set_xdata(sigma_x_list)
        line_sigma_.set_ydata(sigma_y_list)
        line_xi_.set_xdata(xi_x_list)
        line_xi_.set_ydata(xi_y_list)
        text_.set_text(r"$\alpha = {:g}$".format(g.alpha))
        update_segment(g.phi)
    # 
    slider_alpha.on_changed(update_line)

    def update_segment(phi):
        g.phi = phi
        (sigma_x, sigma_y, sigma_z), (xi_x, xi_y, xi_z) = get_sigma_and_xi(phi)
        #
        segment_sigma.set_xdata([0.,sigma_x])
        segment_sigma.set_ydata([0.,sigma_y])
        segment_xi.set_xdata([0.0,xi_x])
        segment_xi.set_ydata([0.0,xi_y])
        fig.canvas.draw_idle()
    # 
    slider_phi.on_changed(update_segment)

    radio_axis = fig.add_axes([0.05, 0.01, 0.15, 0.15])
    radio = RadioButtons(radio_axis, ("cubic", "hexagonal"), active=0 if(g.is_cubic) else 1)

    def cub_hex_change(label):
        g.is_cubic = True if(label=="cubic") else False
        g.is_hexagonal = (not g.is_cubic)
        update_line(g.alpha)
    #
    radio.on_clicked(cub_hex_change)
    # widgets for GUI >>--------------------------------------------------

    ax.legend()

    plt.show()
#



def get_vec_sigma(x, y, z):
    r = np.sqrt(x**2 + y**2 + z**2)
    rx = x/r; ry = y/r; rz = z/r
    
    if(g.is_cubic):
        # cubic
        s = 1 + g.alpha*(4*(rx**4 + ry**4 + rz**4) - 3) # s = sigma/sigma0
    elif(g.is_hexagonal):
        # hexagonal
        s = (1 + g.alpha*((rx**6) - 15*(rx**4)*(ry**2) + 15*(rx**2)*(ry**4) - (ry**6)) \
            + np.abs(g.alpha)*((rz**6) + 5*(rz**4) - 5*(rz**2))) * (1 - (rz**2) + fz*(rz**2))
    #
    sigma = sigma0*s
    x = sigma*rx; y = sigma*ry; z = sigma*rz
    return (x,y,z)
#

def get_gradient_sigma(x, y, z):
    r = np.sqrt(x**2 + y**2 + z**2)
    rx = x/r; ry = y/r; rz = z/r

    if(g.is_cubic):
        # cubic
        # for s = 1 + g.alpha*(4*(rx**4 + ry**4 + rz**4) - 3) # s = sigma/sigma0
        ds_drx = g.alpha*4*(4*rx**3)
        ds_dry = g.alpha*4*(4*ry**3)
        ds_drz = g.alpha*4*(4*rz**3)
    elif(g.is_hexagonal):
        # hexagonal
        # for s = (1 + g.alpha*((rx**6) - 15*(rx**4)*(ry**2) + 15*(rx**2)*(ry**4) - (ry**6)) \
        #         + np.abs(g.alpha)*((rz**6) + 5*(rz**4) - 5*(rz**2))) * (1 - (rz**2) + fz*(rz**2))
        ds_drx = g.alpha*(+6*(rx**5) - 15*4*(rx**3)*(ry**2) + 15*2*rx*(ry**4)) * (1 - (rz**2) + fz*(rz**2))
        ds_dry = g.alpha*(-15*(rx**4)*2*ry + 15*(rx**2)*4*(ry**3) - 6*(ry**5)) * (1 - (rz**2) + fz*(rz**2))
        ds_drz = np.abs(g.alpha)*(6*(rz**5) + 5*4*(rz**3) - 5*2*rz) * (1 - (rz**2) + fz*(rz**2)) \
                + (1 + g.alpha*((rx**6) - 15*(rx**4)*(ry**2) + 15*(rx**2)*(ry**4) - (ry**6)) \
                + np.abs(g.alpha)*((rz**6) + 5*(rz**4) - 5*(rz**2))) * (-2*rz + fz*2*rz)
    #

    val_x = sigma0*((1 - rx*rx)*ds_drx + (0 - rx*ry)*ds_dry + (0 - rx*rz)*ds_drz)/r
    val_y = sigma0*((0 - ry*rx)*ds_drx + (1 - ry*ry)*ds_dry + (0 - ry*rz)*ds_drz)/r
    val_z = sigma0*((0 - rz*rx)*ds_drx + (0 - rz*ry)*ds_dry + (1 - rz*rz)*ds_drz)/r

    return (val_x, val_y, val_z)
#

def get_vec_xi(x, y, z):
    (x,y,z) = get_vec_sigma(x, y, z)
    sigma = np.sqrt(x**2 + y**2 + z**2)

    (grad_sigma_x, grad_sigma_y, grad_sigma_z) = get_gradient_sigma(x, y, z)
    r = np.sqrt(x**2 + y**2 + z**2)
    rx = x/r; ry = y/r; rz = z/r
    # xi vector
    val_x = sigma*rx + r*grad_sigma_x
    val_y = sigma*ry + r*grad_sigma_y
    val_z = sigma*rz + r*grad_sigma_z

    return (val_x, val_y, val_z)

def get_sigma_and_xi(phi_deg):
    theta = np.pi/2; phi = phi_deg*np.pi/180
    rx = np.sin(theta)*np.cos(phi)
    ry = np.sin(theta)*np.sin(phi)
    rz = np.cos(theta)
    (sigma_x, sigma_y, sigma_z) = get_vec_sigma(rx, ry, rz)
    (xi_x, xi_y, xi_z) = get_vec_xi(rx, ry, rz)

    return (sigma_x, sigma_y, sigma_z), (xi_x, xi_y, xi_z)
#



#
if __name__ == "__main__":
    main_xi_plot()