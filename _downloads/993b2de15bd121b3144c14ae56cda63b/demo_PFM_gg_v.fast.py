"""
--------------------------------------------------------------------------------
An Implementation of Multi-Phase-Field Model for Grain Growth
with numpy array operations

                                                                    Jae Sang Lee
                                                                      2023.05.29
Note!
- This Python code is for demonstration purposes and is provided as-is 
  without warranty of any kind. 
- For faster calculations, please optimize the procedures and 
  make a C/C++ or Fortran code.
- For larger systems, a large number of phases is necessary for the phase-field,
  and sparse arrays (selective allocation) should be used as in ref[2].
- For more information, refer to the following articles.
  [1] I.Steinbach and F.Pezzollar, Physica D 134 (1999), 385.
  [2] S.G.Kim et.al., Phy.Rev.E 74 (2006), 061605.
  [3] J.S.Lee et.al. Scrpita Mater. 110 (2016), 113.
--------------------------------------------------------------------------------
"""

import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

# input & setting data
# iM = 128; jM = 128; phM = 3     # for a circular grain (ph = 2) in matrix (ph = 3)
# iM = 128; jM = 128; phM = 4     # for a circular grain (ph = 2) at GB
# iM = 128; jM = 128; phM = 2     # for anti-phase boundary
iM = 128; jM = 128; phM = 32    # for polycrystalline grain growth
dx = 0.2e-6; dy = dx            # [m]
Lambda = 2.5*dx                 # [m], half thickness of interface
sigma_GB = 0.5                  # [J/m^2], GB energy
m_GB = 1.e-10                   # [(m/s)/(J/m^3)], GB mobility
ntime_max = 1000; ntime_mon = 50

# ==============================================================================
def main_pfm_gg():
    # parameters in calculation
    eps = 4/np.pi * np.sqrt(Lambda*sigma_GB)
    w = 2.*sigma_GB/Lambda
    M = np.pi**2/16 * m_GB/Lambda
    dt = 0.7* 1./(2*(eps*eps*M)*(1./(dx*dx)+1./(dy*dy)))
    TOL_phi = 1.e-3
    np.random.seed(0) # for reproducible random numbers
    axis_i = 0; axis_j = 1; axis_ph = 2

    # variables
    phi0 = np.zeros((iM+2,jM+2,phM+1), dtype=np.float64)    # phase-field at t
    phi  = np.zeros((iM+2,jM+2,phM+1), dtype=np.float64)    # phase-field at t+dt

    # for plot
    val2D = np.zeros((iM+2,jM+2), dtype=np.float64)
    plt.style.use("dark_background")                        
    fig_map, ax_map = plt.subplots(1, 1)


    # IC <<---------------------------------------------------------------
    # IC, random phases of ph = 1 ~ phM in cubic array
    seed_size = 4
    for i in range(1,iM+1,seed_size):
        for j in range(1,jM+1,seed_size):
            ph = np.random.randint(1,phM+1)
            for ii in range(seed_size):
                for jj in range(seed_size):
                    phi0[i+ii,j+jj,ph] = 1.0 
                #
            #
        #
    #

    t = 0 #var init
    # IC >>---------------------------------------------------------------

    # time-loop <<--------------------------------------------------------
    for ntime in tqdm(range(1,ntime_max+1)): # ntime = 1 ~ ntime_max
        t = t + dt

        # periodic BC along i = 0 and iM+1
        phi0[0,:,:] = phi0[iM,:,:]
        phi0[iM+1,:,:] = phi0[1,:,:]
        # periodic BC along j = 0 and jM+1
        phi0[:,0,:] = phi0[:,jM,:]
        phi0[:,jM+1,:] = phi0[:,1,:]

        phi0_W = np.roll(phi0,+1,axis=axis_i) # [i,j,ph] shape
        phi0_E = np.roll(phi0,-1,axis=axis_i) # [i,j,ph] shape
        phi0_S = np.roll(phi0,+1,axis=axis_j) # [i,j,ph] shape
        phi0_N = np.roll(phi0,-1,axis=axis_j) # [i,j,ph] shape

        # check co-existing phases and prepare laplacian
        tmp = phi0 + (phi0_W + phi0_E + phi0_S + phi0_N)
        s = (tmp > TOL_phi) # [i,j,ph] shape
        n_coexist = s.sum(axis=axis_ph) # [i,j] shape
        lapl = (phi0_E - 2.*phi0 + phi0_W)/(dx*dx) + (phi0_N - 2.*phi0 + phi0_S)/(dy*dy) # [i,j,ph] shape # 5-pt laplacian

        # calc. phi at t+dt
        for ph in range(1,phM+1): # 1 ~ phM
            term = -1* s[:,:,ph,np.newaxis]*s[:,:,1:phM+1] * M*(0.5*eps*eps*(lapl[:,:,ph,np.newaxis]-lapl[:,:,1:phM+1]) + w*(phi0[:,:,ph,np.newaxis]-phi0[:,:,1:phM+1]))
            sum = np.sum(term, axis=axis_ph) # q!=p is reflected
            #
            dphi_over_dt = -(2./n_coexist)*sum
            phi[:,:,ph] = phi0[:,:,ph] + dphi_over_dt*dt
        #

        # limit phi (remove overflow due to double-obstacle potential)
        one_or_zero = (phi>(1.0-TOL_phi)) # array of [1 if true or 0 if false] with the shape of phi
        phi = one_or_zero + (one_or_zero + phi)*(1-one_or_zero) # phi = 1 if one_or_zero = 1
        one_or_zero = (phi<TOL_phi) # array of [1 if true or 0 if false] with the shape of phi
        phi = (one_or_zero + phi)*(1-one_or_zero) # phi = 0 if one_or_zero = 1 

        # normalize phi, ph = 1 ~ phM, array operations
        phi[:,:,1:phM+1] = phi[:,:,1:phM+1]/(phi[:,:,1:phM+1].sum(axis=axis_ph)[:,:,np.newaxis]) # broadcasting (iM,jM) to (iM,iM,phM) for division

        # phase-field for the next time step
        phi0 = np.copy(phi)

        # monitoring <<---------------------------------------------------
        if(ntime==5 or ntime%ntime_mon == 0):
            tqdm.write("ntime = {:d}, t = {:13.5e}".format(ntime, t)) # tqdm() 사용시 tqdm.write() 로 출력
            # decorate GB regions and plot
            val2D = (phi[:,:,1:phM+1]**4).sum(axis=axis_ph)
            colormesh_val(fig_map, ax_map, val2D, 1,iM, 1,jM, 0.,1., "ntime = {:d}".format(ntime)) # plot (1~iM, 1~jM) 2D matrix data
        #
        # monitoring >>---------------------------------------------------
    #
    # time-loop >>--------------------------------------------------------

    print("OK! End of calculation...")
    plt.show() # for persisting plot
#



# ==============================================================================
def colormesh_val(fig, ax, val2D, il, ih, jl, jh, val_min, val_max, suptitle): 
    """
    plot (il~ih, jl~jh) 2D matrix data
    """
    fig.suptitle("{}".format(suptitle))
    x, y = np.meshgrid(np.arange(il,ih+1), np.arange(jl,jh+1))
    # plot transposed val2D to make i-axis rightward and j-axis upward
    ax.pcolormesh(x, y, np.transpose(val2D[il:ih+1,jl:jh+1]), vmin = val_min, vmax = val_max, shading='auto', cmap='Blues_r') # cmap='RdBu_r', 'Blues_r', 'Greys_r'
    ax.set_aspect('equal', 'box')
    plt.savefig("{}.png".format(suptitle))
    plt.show(block = False); plt.pause(1.e-2)
#



# ==============================================================================
if __name__ == "__main__":
    main_pfm_gg()



# ==============================================================================
"""

iM = 128; jM = 128; phM = 3     # a flat and a sinusoidal boundaries
    # IC, a flat and a sinusoidal boundary
    for i in range(1,iM+1): # 1 ~ iM
        for j in range(1,jM+1): # 1 ~ jM
            i_flat = 0.25*iM
            i_wave = 0.65*iM + 10*np.cos(2*np.pi*(j-1)/(jM-1))
            if (i>=i_flat and i<=i_wave):
                phi0[i,j,2] = 1.0
            else:
                phi0[i,j,3] = 1.0
            #
        #
    #


iM = 128; jM = 128; phM = 3     # for a circular grain (ph = 2) in matrix (ph = 3)
    # IC, a circular grain (ph = 2) in matrix (ph = 3)
    i_center = int(0.4*iM); j_center = int(0.6*jM); nradius = int(0.2*iM)
    for i in range(1,iM+1): # 1 ~ iM
        for j in range(1,jM+1): # 1 ~ jM
            if ((i-i_center)**2+(j-j_center)**2 <= nradius**2):
                phi0[i,j,2] = 1.0
            else:
                phi0[i,j,3] = 1.0
            #
        #
    #


iM = 128; jM = 128; phM = 4 # for a circular grain (ph = 2) at GB
    # IC, a circular grain (ph = 2) at GB 
    i_center = int(0.5*iM); j_center = int(0.5*jM); nradius = int(0.2*iM)
    for i in range(1,iM+1): # 1 ~ iM
        for j in range(1,jM+1): # 1 ~ jM
            if ((i-i_center)**2+(j-j_center)**2 <= nradius**2):
                phi0[i,j,2] = 1.0
            else:
                if(j<=j_center):
                    phi0[i,j,3] = 1.0
                else:
                    phi0[i,j,4] = 1.0
                #
            #
        #
    #


iM = 128; jM = 128; phM = 2     # for anti-phase boundary
iM = 128; jM = 128; phM = 32    # for polycrystalline grain growth
    # IC, random phases of ph = 1 ~ phM in cubic array
    seed_size = 4
    for i in range(1,iM+1,seed_size):
        for j in range(1,jM+1,seed_size):
            ph = np.random.randint(1,phM+1)
            for ii in range(seed_size):
                for jj in range(seed_size):
                    phi0[i+ii,j+jj,ph] = 1.0 
                #
            #
        #
    #

"""


