"""
A simple 1-D phase-field model

                                                                    Jae Sang Lee
                                                        2024.10 Since 2023.05.29
"""
import numpy as np
import matplotlib.pyplot as plt

lamb        = 0.5*1e-9
sigma       = 0.3
m           = 1.0e-10
delta_f     = 1e7 #xxx 0, 1e7 # an arbitrary driving force for the interface migration

iM          = 100
dz          = lamb/10   # 2*10 grids in the interface region
ntime_max   = 100 #xxx 100, 20000
ntime_mon   = 10 #xxx 10, 2000

z       = np.zeros(iM+2)
phi     = np.zeros(iM+2)
phi0    = np.zeros(iM+2)

omega       = sigma/(2*lamb)*4
eps_sq      = (2*lamb)*sigma*8/(np.pi**2)
M           = m/(2*lamb)*(np.pi**2)/8

# IC, a step profile
for i in range(1,iM+1):
    z[i] = (i-0.5)*dz
    phi0[i] = 0. if(i<0.5*iM) else 1.0
#

dt = 0.7* 1./(2*(m*sigma)) *(dz*dz)

print("omega, eps_sq, M: {:13.5e}, {:13.5e},{:13.5e}".format(omega, eps_sq, M))
print("delta_f: {:13.5e}".format(delta_f))
print("dz, dt: {:13.5e}, {:13.5e}".format(dz, dt))

# plot initial phi-profile
plt.subplots(figsize=(7, 4.5))
plt.subplots_adjust(left = 0.1, right=0.75, bottom=0.1, top=0.9)
plt.xlim(1e-9,4e-9)
plt.plot(z[1:iM],phi0[1:iM], label="initial")

# time-loop <<------------------------------------------------------------
for ntime in range(1,ntime_max+1): # 1 ~ ntime_max
    # adiabatic BC
    phi0[0] = phi0[1]
    phi0[iM+1] = phi0[iM]
    # calc. phi(t+dt) from phi(t)
    for i in range(1,iM+1): # 1 ~ iM
        lapl = (phi0[i+1] - 2*phi0[i] + phi0[i-1])/(dz*dz)
        dg_dphi = -(2*phi0[i]-1)
        dh_dphi = 6*phi0[i]*(1-phi0[i])
        dphi_dt = M*(eps_sq*lapl - omega*dg_dphi - delta_f*dh_dphi)
        phi[i] = phi0[i] + dphi_dt * dt
        # limit phi, due to double obstacle potential
        if(phi[i]>1.0): phi[i] = 1.0
        elif(phi[i]<0.0): phi[i] = 0.0
    #
    for i in range(1,iM+1):
        phi0[i] = phi[i]
    #
    if(ntime%ntime_mon==0):
        print("ntime = {}".format(ntime))
        plt.plot(z[1:iM],phi[1:iM], label="ntime = {}".format(ntime))
        plt.show(block = False); plt.pause(1.e-2)
    #
#
# time-loop >>------------------------------------------------------------
# emphasize the final phi-profile
itmp = int(0.5*iM)
plt.plot(z[1:iM],phi[1:iM], linestyle="None", marker="o", c="#000000", markersize=5)
plt.legend(bbox_to_anchor=(1.02, 1.0), loc='upper left', borderaxespad=0.)
plt.show(block = False); plt.pause(1.e-2)

plt.savefig("zz_result.png")
plt.show()